#!/usr/bin/env bash

# DIR_NAME=$(dirname "${BASH_SOURCE}")
DIR_SCRIPT_NAME=$(readlink -f -- $(dirname "${BASH_SOURCE}"))

function show_output() {
  echo "### ${1}"
}

function show_output_nb() {
  echo -n "### ${1}"
}

function show_usage() {
  echo "  "
  echo "Usage: ${0} --image IMAGE --tag TAG --test TEST"
  echo "  "
  echo "    --image IMAGE    - Name of image supported by distrobox"
  echo "                       - Ex: fedora, ubuntu, archlinux"
  echo "                       - Default: ubuntu"
  echo "                       - List images: --images"
  echo "    --tag TAG        - The image's release tag"
  echo "                       - Default: latest"
  echo "    --test TEST      - The test script to execute within distrobox"
  echo "    --images         - Show list of available images:tags"
  echo "                       - Same as: distrobox-create --compatibility"
  echo "    -h --help        - This help"
  echo "  "
}

function install_distrobox() {
  echo "Installing distrobox...."

  DB_VER="1.4.2"
  DB_URL="https://raw.githubusercontent.com/89luca89/distrobox/${DB_VER}/install"
  INST_PREFIX="${HOME}/.local"

  read -r -d '' OUTPUT <<-EOF
---
Downloading: ${DB_URL}
Installing to: ${INST_PREFIX}
---
EOF

  echo "$OUTPUT"

  curl -sN "${DB_URL}" | sh -s -- --prefix "${INST_PREFIX}"
}


##########################
# Check/Install distrobox
##########################
if ! which distrobox >/dev/null 2>&1; then
  show_output "ERROR: unable to find distrobox in you PATH"
  show_output " "
  show_output "This script can attempt to install it under \$HOME/.local/bin'"
  show_output_nb "Would you like to install it? [y/N] "
  
  read answer

  case "$answer" in
    y|Y)
      install_distrobox
      ;;
    *)
      exit
      ;;
  esac
fi


#####################
# Default values
#####################
DB_IMAGE="ubuntu"
DB_TAG="latest"
ANS_TEST=""


#####################
# Parse args
#####################
while (( "$#" )); do
  case "$1" in
    --image)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        DB_IMAGE=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;
    --tag)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        DB_TAG=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;
    --test)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        ANS_TEST=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;
    --images)
      distrobox-create --compatibility
      exit 0
      ;;
    -h|--help)
      show_usage
      exit 0
      ;;
    -*|--*=) # unsupported flags
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
    *) # preserve positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done


if [ -z "$ANS_TEST" ]; then
  show_output "ERROR: missing 'test' argument"
  show_usage
  exit 1
fi

if [ -z "$DB_TAG" ]; then
  TAG="latest"
fi

DB_NAME="${DB_IMAGE}-${DB_TAG}"
show_output "DB_NAME: ${DB_NAME}"

DB_HOME=$(readlink -f "${DIR_SCRIPT_NAME}/../")
show_output "DB_HOME: ${DB_HOME}"

DB_VOLUME_ANS=$(readlink -f "${DIR_SCRIPT_NAME}/../")
show_output "DB_VOLUME_ANS: ${DB_VOLUME_ANS}"

IMAGE_EXISTS=$(distrobox list | tail -n +2 | awk -F"|" '{ for(i=1; i<=NF; i++) {gsub(/^[ \t]+|[ \t]+$/, "", $i)} print }' | grep -q "${DB_NAME}" ; echo $?)


function create_box() {
  DB_INIT="/ansible/tests/init-${DB_IMAGE}.sh"
  show_output "DB_INIT: ${DB_INIT}"

  distrobox create --yes --image "$DB_IMAGE:$DB_TAG" --name "$DB_NAME" --volume "${DB_VOLUME_ANS}":/ansible --init-hooks "sh /ansible/tests/init-${DB_IMAGE}.sh"
}


if [ $IMAGE_EXISTS -ne 0 ]; then
  create_box
else
  show_output "Image ${DB_IMAGE}:${DB_TAG} already exists"
fi

distrobox enter --name $DB_NAME -- "/ansible/tests/test-${ANS_TEST}.sh"
