#!/usr/bin/env bash

DIR="$(dirname -- "$(readlink -f "${BASH_SOURCE}")")"
. "${DIR}/.db-check" && exit

DB_VER="1.4.2"
DB_URL="https://raw.githubusercontent.com/89luca89/distrobox/${DB_VER}/install"

INST_PREFIX="${HOME}/.local"

read -r -d '' OUTPUT <<-EOF
---
Downloading: ${DB_URL}
Installing to: ${INST_PREFIX}
---
EOF

echo "$OUTPUT"

curl -sN "${DB_URL}" | sh -s -- --prefix "${INST_PREFIX}"
