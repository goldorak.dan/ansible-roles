#!/usr/bin/env bash

echo "executing test.sh"

if [[ "registubi" =~ "/" ]]; then
  echo "yes"
else
  echo "no"
fi
exit


IN="quay.io/rockylinux/rockylinux:8-minimal"
IFS='/' read -r -a array <<< "$IN"
IFS=':' read -r -a release <<< "${array[-1]}"

echo ${release[1]} 
